# Software Studio 2021 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : Midterm project

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

# �@�~���}�Ghttps://midterm-108062326.web.app/

## Website Detail Description
![](https://i.imgur.com/6xKxrIS.png)

This is a chat room.


# Components Description : 
1. Side navbar : Change between rooms
2. Top navbar : Display username and logout button
3. Input : Send texts

# Other Functions Description : 
1. Images uploading : Send images
