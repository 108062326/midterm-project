// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
var firebaseConfig = {
    apiKey: "AIzaSyCNiFJmkzB1HRYXDNHvycjKkgUSUIMUWIA",
    authDomain: "midterm-108062326.firebaseapp.com",
    databaseURL: "https://midterm-108062326-default-rtdb.firebaseio.com",
    projectId: "midterm-108062326",
    storageBucket: "midterm-108062326.appspot.com",
    messagingSenderId: "919086218154",
    appId: "1:919086218154:web:6fedbee142ab8de77509d3",
    measurementId: "G-12T740Z8Z9"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();