window.onload = function () {
    var user_email = firebase.auth().currentUser;
    var username = "";
    var notificationsOn = false;
    firebase.auth().onAuthStateChanged(function (user) {
        var navItems = document.getElementById("navitems");
        if (user) {
            // console.log(firebase.auth().currentUser);
            user_email = user.email;
            username = user_email.split("@")[0];
            console.log(username);
            // console.log(user_email);
            navItems.innerHTML = "<span class='navbar-text'>" + user_email + '</span ><li class="nav-item"><a class="nav-link" href="#" id="btnLogout">Log out</a></li><li class="nav-item d-md-none"><a href="#" class="nav-link link-dark" id="createRoomTop">Create or Join</a></li><li class="nav-item"><a href="#" class="nav-link active room d-md-none" id="publicRoomTop">Public room</a></li>';
            var createRoomTop = document.getElementById("createRoomTop");
            createRoomTop.addEventListener("click", () => {
                roomsName = prompt("Enter the room's name!");
                createRoom(roomsName);
            })
            var publicRoomTop = document.getElementById("publicRoomTop");
            publicRoomTop.addEventListener("click", () => {
                changeRoom(publicRoom);
            })
            var btnLogout = document.getElementById("btnLogout");
            btnLogout.addEventListener("click", function () {
                firebase.auth().signOut().then(function () {
                    alert("User sign out success!");
                }).catch(function (error) {
                    alert("User sign out failed!");
                })
                document.getElementById("chatArea").innerHTML = "";
                document.getElementById("side-navbar").innerHTML = '<ul class="nav nav-pills flex-column mb-auto" id="rooms"><li class="nav-item"><a href="#" class="nav-link link-dark" id="createRoom">Create room</a> </li><li class="nav-item"><a href="#" class="nav-link active room" id="publicRoom">Public room</a></li> </ul>';
            }, false);
            firebase.database().ref("users/" + username + "/rooms").once("value").then((snapshot) => {
                snapshot.forEach((childshot) => {
                    console.log("looping");
                    var childData = childshot.val();
                    createRoomBtn(childData);
                })
                changeRoom(publicRoom);
            })
            if (Notification.permission === "granted") {
                const notification = new Notification("New message!", {
                    body: "Hi there!"
                })
                notificationsOn = true;
            }
            else if (Notification.permission !== "denied") {
                Notification.requestPermission().then((permission) => {
                    if (permission === "granted") {
                        const notification = new Notification("New message!", {
                            body: "Hi there!"
                        })
                        notificationsOn = true;
                    }
                })
            }
        }
        else {
            navItems.innerHTML = '<li class="nav-item"><a class="nav-link" href="signin.html" id="btnLogin">Login</a></li>'
        }
    })

    var currentRoomRef = firebase.database().ref("rooms/Public room/messages");
    // console.log(currentRoomRef);
    // console.log(user_email);
    // console.log(firebase.auth().currentUser);
    var textBox = document.getElementById("textBox");
    var sendBtn = document.getElementById("sendBtn");
    var chatArea = document.getElementById("chatArea");
    sendBtn.addEventListener("click", function () {
        if (textBox.value != "") {
            // If the message contains html tags
            var str = textBox.value;
            str = str.replaceAll("&", "&amp");
            str = str.replaceAll("<", "&lt");
            str = str.replaceAll(">", "&gt");
            textBox.value = str;
            var message = {
                sender: user_email,
                content: textBox.value,
                room: currentActiveRoom[0].innerHTML,
                url: "",
                type: "text"
            };
            currentRoomRef.push(message);
            textBox.value = "";
            chatArea.scrollTop = chatArea.scrollHeight;
        }
    })

    var str_my_message = '<div class="d-flex justify-content-end text-white">';
    var str_otherppls_message = '<div class="d-flex justify-content-start text-white">'
    var str_before_useremail = '<div class="animation my-2 p-1 rounded shadow-sm"><p class="p-1 mb-0 small lh-125"><strong class="d-block">';
    var str_after_useremail = '</strong>';
    var str_before_img = "<img class='img pt-2' style='width: 100%;' src='";
    var str_after_img = "'>";
    var str_after_message = '</p></div></div>';
    var totalMessages = [];
    // function loadExistingMessages() {
    console.log("loadExistingMessages function  triggered");
    // A problem occurs here.
    // After window loading, it seems like both once and on are triggered.
    // Does it mean that we actually don't need once?
    // currentRoomRef.once("value").then((snapshot) => {
    //     console.log("Once triggered.");
    //     snapshot.forEach((childshot) => {
    //         var childData = childshot.val();
    //         console.log(childData);
    //         var sender = (childData.sender === "") ? "Anonymous" : childData.sender;
    //         totalMessages[totalMessages.length] = str_before_username + sender + str_after_username + childData.content + str_after_message;
    //         // console.log(sender);
    //         // console.log(childData.content);
    //         document.getElementById("chatArea").innerHTML = totalMessages.join("");
    //     });
    // })
    //update messagess upon sending
    var roomsName = "public";
    var roomPath = "rooms/" + roomsName + "/messages";
    // Set up public room updating function
    currentRoomRef.on("child_added", (data) => {
        console.log("On triggered.");
        // console.log(user_email);
        var childData = data.val();
        // console.log(data.val());
        if (childData.room == currentActiveRoom[0].innerHTML) {
            if (childData.sender == user_email) {
                totalMessages[totalMessages.length] = str_my_message + str_before_useremail + childData.sender + str_after_useremail + str_before_img + childData.url + str_after_img + childData.content + str_after_message;
            }
            else {
                totalMessages[totalMessages.length] = str_otherppls_message + str_before_useremail + childData.sender + str_after_useremail + str_before_img + childData.url + str_after_img + childData.content + str_after_message;
                if (notificationsOn == true) {
                    console.log("test notification");
                    if (childData.type == "text") {
                        const notification = new Notification("New message!", {
                            body: childData.sender + ": " + childData.content
                        })
                    }
                    else if (childData.type == "image") {
                        const notificatiob = new Notification("New message", {
                            body: childData.sender + ": " + "*image*"
                        })
                    }
                }
            }
        }
        // console.log(data.val().sender);
        document.getElementById("chatArea").innerHTML = totalMessages.join('');
        chatArea.scrollTop = chatArea.scrollHeight;
    })

    function createRoomBtn(roomsName) {
        // var rooms = document.getElementById("sideBarRooms");
        // var newRoomLi = document.createElement("li");
        // newRoomLi.classList.add("nav-item");
        // var newRoomA = document.createElement("a");
        // newRoomA.setAttribute("href", "#");
        // newRoomA.classList.add("nav-link");
        // newRoomA.classList.add("active");
        // newRoomA.classList.add("room");
        // newRoomA.innerHTML = roomsName;
        // newRoomLi.appendChild(newRoomA);
        // newRoomLi.addEventListener("click", () => {
        //     changeRoom(newRoomA);
        // });
        // rooms.appendChild(newRoomLi);
        var sideBarRooms = document.getElementById("sideBarRooms");
        var newSideRoomLi = document.createElement("li");
        newSideRoomLi.classList.add("nav-item");
        var newSideRoomA = document.createElement("a");
        newSideRoomA.setAttribute("href", "#");
        newSideRoomA.classList.add("nav-link");
        newSideRoomA.classList.add("active");
        newSideRoomA.classList.add("room");
        newSideRoomA.innerHTML = roomsName;
        newSideRoomLi.appendChild(newSideRoomA);
        newSideRoomLi.addEventListener("click", () => {
            changeRoom(newSideRoomA);
        });
        sideBarRooms.appendChild(newSideRoomLi);
        // Add new room to the top navbar
        var topBarRooms = document.getElementById("navitems");
        var newTopRoomLi = document.createElement("li");
        newTopRoomLi.classList.add("nav-item");
        newTopRoomLi.classList.add("d-md-none");
        var newTopRoomA = document.createElement("a");
        newTopRoomA.setAttribute("href", "#");
        newTopRoomA.classList.add("nav-link");
        newTopRoomA.classList.add("active");
        newTopRoomA.innerHTML = roomsName;
        newTopRoomLi.appendChild(newTopRoomA);
        newTopRoomLi.addEventListener("click", () => {
            changeRoom(newTopRoomA);
        })
        topBarRooms.appendChild(newTopRoomLi);

        changeRoom(newSideRoomA);
        currentRoomRef.on("child_added", (data) => {
            console.log("changeRoom on triggered.");
            var childData = data.val();
            if (childData.room == currentActiveRoom[0].innerHTML) {
                if (childData.sender == user_email) {
                    totalMessages[totalMessages.length] = str_my_message + str_before_useremail + childData.sender + str_after_useremail + str_before_img + childData.url + str_after_img + childData.content + str_after_message;
                }
                else {
                    totalMessages[totalMessages.length] = str_otherppls_message + str_before_useremail + childData.sender + str_after_useremail + str_before_img + childData.url + str_after_img + childData.content + str_after_message;
                    if (notificationsOn == true) {
                        console.log("test notification");
                        if (childData.type == "text") {
                            const notification = new Notification("New message!", {
                                body: childData.sender + ": " + childData.content
                            })
                        }
                        else if (childData.type == "image") {
                            const notificatiob = new Notification("New message", {
                                body: childData.sender + ": " + "*image*"
                            })
                        }
                    }
                }
                document.getElementById("chatArea").innerHTML = totalMessages.join('');
                chatArea.scrollTop = chatArea.scrollHeight;
            }
        })
    }

    function createRoom(roomsName) {
        if (roomsName != null && roomsName != "") {
            var isRepeated = false;
            firebase.database().ref("users/" + username + "/rooms").once("value").then((snapshot) => {
                snapshot.forEach((childshot) => {
                    var childData = childshot.val();
                    if (roomsName == childData) isRepeated = true;
                })
            })
            if (isRepeated == false) {
                firebase.database().ref("users/" + username + "/rooms").push(roomsName);
                createRoomBtn(roomsName);
            }
        }
    }

    var btnCreateRoom = document.getElementById("createRoom");
    btnCreateRoom.addEventListener("click", () => {
        roomsName = prompt("Enter the room's name!");
        createRoom(roomsName);
    })

    // A serious problem occurs here.
    // At every time changeRoom being called, it generates another database .on function instance.
    // In other words, if the room "test room" is clicked 5 times, then when you send a message,
    // there will be 5 copies!
    // Solution: move on to the end of createRoom
    // 每次changeRoom被呼叫時，databse的on function都會被宣告一次，可以說listener多了一個，
    // 若點擊"test room" 5次，那麼每次在test room輸入訊息都會跑出5個!
    // 得想辦法讓on function只被宣告一次。
    // 解法: 將on移動到createRoom的最下方。
    var currentActiveRoom = document.querySelectorAll(".nav-link.active");
    function changeRoom(room) {
        if (room.innerHTML == "Public room") {
            console.log("switched to public room");
        }
        else {
            console.log("not public room");
        }
        // querySelectorAll can select an element with more than one classes,
        // while querySelector can only select one.
        // querySelectorAll's return value is a list of elements, so here I added an index 0.
        currentActiveRoom = document.querySelectorAll(".nav-link.active");
        for (var i = 0; i < currentActiveRoom.length; i++) {
            currentActiveRoom[i].classList.remove("active");
            currentActiveRoom[i].classList.add("link-dark");
        }
        room.classList.add("active");
        room.classList.remove("link-dark");
        currentActiveRoom = document.querySelectorAll(".nav-link.active");
        roomPath = "rooms/" + room.innerHTML + "/messages";
        currentRoomRef = firebase.database().ref(roomPath);
        totalMessages = [];
        document.getElementById("chatArea").innerHTML = "";
        // Load existing messages
        // console.log(isTheRoomFirstTimeBeingCalled.get(room.innerHTML));
        currentRoomRef.once("value").then((snapshot) => {
            console.log("once triggered");
            // return new Promise((resolve) => {
            snapshot.forEach((childshot) => {
                var childData = childshot.val();
                if (childData.room == currentActiveRoom[0].innerHTML) {
                    if (childData.sender == user_email)
                        totalMessages[totalMessages.length] = str_my_message + str_before_useremail + childData.sender + str_after_useremail + str_before_img + childData.url + str_after_img + childData.content + str_after_message;
                    else
                        totalMessages[totalMessages.length] = str_otherppls_message + str_before_useremail + childData.sender + str_after_useremail + str_before_img + childData.url + str_after_img + childData.content + str_after_message;
                    document.getElementById("chatArea").innerHTML = totalMessages.join("");

                    chatArea.scrollTop = chatArea.scrollHeight;
                }
            });
            // })
        })
    }

    var publicRoom = document.getElementById("publicRoom");
    publicRoom.addEventListener("click", () => {
        changeRoom(publicRoom);
    })

    var imgBtn = document.getElementById("imgBtn");
    imgBtn.addEventListener("change", () => {
        const fileImage = imgBtn.files[0];
        var storageRef = firebase.storage().ref();
        var imageRef = storageRef.child(fileImage.name);
        imageRef.put(fileImage).then(() => {
            imageRef.getDownloadURL().then((url) => {
                var message = {
                    sender: user_email,
                    content: "",
                    room: currentActiveRoom[0].innerHTML,
                    url: url,
                    type: "image"
                }
                currentRoomRef.push(message);
            })
        })
    })



    // I don't know why user_email becomes empty
    // Okay now I know why.
    // user_email's value is given in the onAuthStateChanged function,
    // which is an asynchronous function, executed after all the other instructions completed.
    // So at this moment (line 185) user_email has not yet be given any value.
    // Possible solution: wrap this createRoomBtn function in to the onAuthStateChanged function at beginning
    // empty user_email makes createRoomBtn malfunctioned.
    // // Load every room the user is in.
    // firebase.database().ref("users/" + user_email + "/rooms").once("value").then((snapshot) => {
    //     snapshot.forEach((childshot) => {
    //         console.log("looping");
    //         var childData = childshot.val();
    //         createRoomBtn(childData);
    //     })
    //     changeRoom(publicRoom);
    // })
    // Mind that asynchronous functions are executed in the end of program.
    // So putting changeRoom here isn't gonna be working.
    // changeRoom(publicRoom);
}

// 4/21已知BUG: 登入後不知為何又被登出
// 第7行的log可以顯示user_email
// 第26行的log顯示不出user_email
// 但59行的能夠顯示!?
// 此外，將database的規則改為登入後才能讀寫的話，
// 就算有登入也一樣看不到東西，
// 由此可知應該是因某種原因導致被登出。